﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielHimsani_HW18
{
    public partial class EX1 : Form
    {
        //this file is the users.txt file, i open it here because i want to get accsess from all the fuctions
        System.IO.StreamReader file = new System.IO.StreamReader(@"C:\Users\magshimim\Documents\magshimim\Classes\11grade\C++\L19\DanielHimsaniProject - HW18\DanielHimsani-HW18\Users.txt");

        public EX1()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;//lock the resize of the form
        }

        private void button2_Click(object sender, EventArgs e)
            //this is the enter button
        {
            bool flag = false;//this flag change to true if the user succeeded to connect
            string line = "";//the line string, i read the file line by line
            string name = "";
            while ((line = file.ReadLine()) != null)//read the file while its not the end
            {
                //split fuction - give me the first part of the line by split with ',' char.
                //so, split(',')[0] wiil give me the name and [1] give me his password
                if(line.Split(',')[0]== NameTextBox.Text && line.Split(',')[1] == PasswordTextBox.Text)//if _name == name && _password==password
                {
                    name = line.Split(',')[0];//save this name
                    flag = true;//turn on the flag
                }
            }
            file.Close();
            if (flag)//if the user succeeded to connect
            {
                Global.Name = name;//save the user name into the static global class
                Form Celender = new CelenderForm();//create the new form of the celender
                this.Hide();//hide this form
                Celender.ShowDialog();//showdialog mean that the celender form will run when this form will close
                this.Close();//close this form                              
            }
            else//the user failed to connect
            {
                MessageBox.Show("שם משתמש וסיסמא לא נכונים");
                NameTextBox.Text = "";//clear the name text box
                PasswordTextBox.Text = "";//clear the password text box
            }
            
        }

        private void button1_Click(object sender, EventArgs e)//if the cancel button were clicked, close this form.
        {
            this.Close();
        }
    }
}
