﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace DanielHimsani_HW18
{
    public partial class CelenderForm: Form
    {
        //this string is the path of the connected user text files with all his saves birthdays
       string path ="C:\\Users\\magshimim\\Documents\\magshimim\\Classes\\11grade\\C++\\L19\\DanielHimsaniProject - HW18\\DanielHimsani-HW18\\" + Global.Name +"BD.txt";
        public CelenderForm()
        {
            InitializeComponent();
            MsgLabel.Left = 0;//set the msglabel x to 0
            this.FormBorderStyle = FormBorderStyle.FixedSingle;//lock the resize of the form

        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
            //this fuction dtart when the user change the selected date on the celenter
        {
            //now, in need to chack the selected date from the celender, then check if someone has a birthday
            //on that day with reading the user text file

            string line = "";//the line string
            bool flag = true;//this flag will turn off when the while loop find birthday, than stop searching
            bool flag2 = true;//this flag will stay on when the while loop didnt find birthday on the selected date, then gice the user a message.

            using (StreamReader file = new StreamReader(path))//open the file with stream reader, only with this stream its easy to read line by line
            {
                file.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);//set the file seek to the begining of the file
                while ((line = file.ReadLine()) != null && flag)//read line by line while its not the end of the file and you dont find mach birthday
                {
                    DateTime date = monthCalendar1.SelectionRange.Start;//get the selected date from the celender
                    string dateStr = date.Month.ToString() + "/" + date.Day.ToString() + "/" + date.Year.ToString();//convert the date to the wanted format
                    if (line.Split(',')[1] == dateStr)//if we find match date
                    {
                        MsgLabel.Text = "בתאריך הנבחר " + line.Split(',')[0] + "!חוגג יום הולדת";//give the user the message
                        flag = false;
                        flag2 = false;
                    }
                }
            }
            if(flag2)//if the while loop dosent find a matching date
            {
                MsgLabel.Text = "התאריך הנבחר - אף אחד לא חוגג יום הולדת";
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
            //this fuction will start when the user want to add a birthdate to the database
        {
            string name = NameTextBox.Text;//save the new birthday name
            DateTime dt = dateTimePicker1.Value.Date; //get the selected date from the date picker
            string date  = dt.Month.ToString() + "/" + dt.Day.ToString() + "/" + dt.Year.ToString();//convert the date to the wanted format
            bool isInEnglish = true;
            //this loop check if all the digits in the name is in english
            for (int i = 0; i<name.Length;i++)
            {
                if(!((name[i] <= 'z' && name[i]>='a') || (name[i] <= 'Z' && name[i] >= 'A')))
                {
                    isInEnglish = false;
                }             
            }

            if(!isInEnglish || NameTextBox.Text == "")//if the for loop find invalid char
            {
                MessageBox.Show(" הכנס שם באנגלית");//show the user this error
                NameTextBox.Text = "";//clear the text box and give the user another chance
            }
            else//if the name is valid and english digits
            {
                if(isDateExiest(date))// the input date is allready taken
                {
                    MessageBox.Show("מישהו חוגג כבר בתאריך זה");//show the user this message
                }
                else//this date is empty, reason for party
                {
                    using (StreamWriter file = File.AppendText(path))//open the file in a stream writer with the append mode
                    {
                        file.Write("\n"+name + "," + date);
                    }
                }
                NameTextBox.Text = "";//clear the text box to another user input
            }
        }

        private bool isDateExiest(string date)
            //this fuction input is string
            //this fuction search in the user file if the input date is allready taken
        {
            using (StreamReader file = new StreamReader(path))//open the user text file with stream reader for reading line by line
            {
                file.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);//set the file seek to the begining of the file
                string line = "";
                while ((line = file.ReadLine()) != null)//read untl the end of the file
                {
                    if (line.Split(',')[1] == date) return true;//if we find match date, return true
                }
                return false;//the while loop dosent find a matching date, so return false
            }
        }
    }
}
